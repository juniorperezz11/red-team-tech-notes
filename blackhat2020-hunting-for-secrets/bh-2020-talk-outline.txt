﻿1. What is this about?
   1. I want to share with you two tools.  Gitrob, which we forked from Michael Henrikson’s excellent tool to add some cool features, and Token-Hunter which we wrote internally.  
   2. More than that, I want to give you enough information to begin hunting secrets in your own environments, because context is nearly the most important thing in hunting secrets.  What types of tokens are you looking for?  Where do they typically show up?  What are their makeup so that I can find them??  How do I know what they have access to?  All of these questions are context-specific.
   3. Secrets like API tokens, encryption keys, usernames, and passwords are chased by all kinds of attackers with a range of different goals for an “easy” win to gain initial access.  I use air quotes for easy because, in most cases it’s a harder problem than simply running a tool to be really effective at it.  Even harder at scale.  
   4. I’m hoping to share some practical knowledge about how these types of tools work, and how to safely learn about what you’ve found when you do find something interesting.
2. Who am I?
   1. Welcome to my arsenal demo on token-hunter and gitrob:  hunting for secrets.
   2. Greg Johnson
   3. Senior Security Engineer, Red Team @ GitLab
   4. I love my job.  I like hacking and building software.  Sometimes I get to do both at the same time which is really fun for me.
   5. Reach out on twitter if you like!
3. But why - why did we create these tools?
   1. Gitrob and Token-Hunter are complementary tools for looking for secrets.  We created each one to support red team operations at GitLab.  Git repositories are a well-known vector for secrets leaks, however, there was very little support in existing open source tooling for GitLab specifically when we began.
   2. We started really small by creating bash scripts to clone and scan GitLab repositories, had some success, and then began iterating and expanding on the idea.  
   3. For historical scanning, Gitrob was the perfect stepping stone to get where we wanted to go rather quickly.  We liked the web interface aspect of the tool, the fact it was written in Go and the fact it had an OSINT mechanism for discovering common, relevant leak sources in places that would also apply to GitLab.
   4. Regarding Token-Hunter, we had the idea early on to begin experimenting with more innovative techniques for finding secrets leaks in other areas of GitLabs assets:  issues, issue discussions, merge request and their discussions, snippets, and most recently CI job logs.  Token-Hunter was created as a result of these efforts.
4. What is Gitrob?
   1. Gitrob is a popular secrets scanning tool that works in a unique way.
   2. It clones repositories to a configurable historical depth and looks at each individual commit for interesting files using string and pattern matching.
   3. Many tools work in a similar way using different techniques to find secrets during the scanning process.
   4. To understand how historical scanning tools work, it’s important to understand how git stores data for a project.  
   5. As a quick reminder before we get into the tools, git stores data as a stream of snapshots over time.  Every time you save the state of your project, git stores a picture of what each file looks like.  Unchanged files are referenced instead of duplicated and if a file changes, a patch containing just what changed is stored.
5. New gitrob features we added
   1. Gitrob:  we added a ton of features.
      1. Broadly speaking, we added support for GitLab, multiple modes for scanning for secrets, performance enhancements related to the repository cloning process, and docker support for automation efforts we made.
      2. Both file and content signatures are no longer compiled into the binary making it easier to edit and adjust the scope of your scans on the fly without the need to recompile the tool.
      3. Lets look at a demo to better understand some of the new features specifically.
6. How do we find secrets?
   1. A little about the OSINT mechanism and semantics behind both tools.  What I mean by OSINT is the way the tool discovers projects, members, and members’ projects starting with an organization or group.
   2. For OSINT, both tools are similar in the way they have the ability to expand from a group or organization to its related assets.  Starting with a group, each tool finds related projects and members and optionally expands to searching members personal projects and their related assets.
7.  Gitrob Demo Script:
   1. git clone https://github.com/codeEmitter/gitrob.git
   2. cd ./gitrob
   3. go build
   4. ./gitrob -h
   5. export GITROB_GITLAB_ACCESS_TOKEN=W_KzztMxeEmcK6kAUz5R
   6. ./gitrob -in-mem-clone -save ./demo1.json -debug 8588444
   7. ls -l *.json
   8. cat ./demo1.json | jq
   9. vim ./filesignatures.json
   10. vim ./contentsignatures.json
   11. ./gitrob -in-mem-clone -mode 2 -save ./demo2.json -debug 8588444 (insert secret in secrets.yml)
   12. ./gitrob -in-mem-clone -mode 3 -save ./demo3.json -debug 8588444 (insert content secret in repo)
      1. Tangentially, one thing to note here is that deleted commits are a common source of valid secrets given a content match is in play.  Git is a complex tool with a steep learning curve.  What sometimes happens is a commit with a sensitive piece of information will be made, and then later deleted forming another commit.  Though this removes the sensitive information from the tip of a given branch, previous commits in the repositories history still contain the sensitive information.
      2. docker build . -t gitrob:latest
      3. docker run -v /tmp/output:/tmp/output gitrob:latest -save ./output/demo4.json -mode 3 -gitlab-access-token W_KzztMxeEmcK6kAUz5R -bind-address 0.0.0.0 8588444
8. Token-Hunter:  A bit more simple to explain, but also effective during our operations.  My teammate, Chris Moberly, initially created it to perform OSINT on GitLab groups to inform other tools we were working with.  We then expanded on the idea.  It’s a complementary tool to gitrob to search GitLab-specific assets like issues, merge requests, snippets, and CI job logs for sensitive data.  In other words, what exposures are we seeing on the periphery of the git repository itself?  
9. You can also pipe your traffic through a proxy, local or otherwise, to record the traffic.  This is helpful for red team assessments or penetration tests where you want to be able to show defenders traffic patterns they might see while being scanned by a tool like this.  Maybe they would like to defend against it.
10. We have support for GitLab, both gitlab.com and self-hosted only right now with token-hunter, however, we’re open to accepting merge requests to support other git hosts.  Please!  Anyone can contribute.
11. Token-Hunter Demo:
   1. clear
   2. git clone https://gitlab.com/gitlab-com/gl-security/gl-redteam/token-hunter.git
   3. cd ./token-hunter
   4. pip3 install -r requirements.txt
   5. ./token-hunter.py -h
   6. cat ./regexes.json | jq
   7. export GITLAB_API_TOKEN=W_KzztMxeEmcK6kAUz5R
   8. ./token-hunter.py -g 8588444
      1. OSINT Show group projects and members of the group
   9. ./token-hunter.py -m -g 8588444
      1. OSINT Show group projects, members of the group and their personal projects
   10. ./token-hunter.py -misr -g 8588444
      1. Show snippet, issue, and merge request
12. Eureka!!!  Now what?
   1. From an offensive perspective, checking the access of a found token can be tricky and dangerous. 
   2. Most tokenization platforms don’t have a direct channel to check what the token has access to.  Not to mention SSH private keys and the like.  Sometimes you’re lucky and find enough context to start, but it can be a daunting task.
   3. I’ve written some light-weight tooling that checks for access levels on found AWS tokens using dry-run flags on operations that check for write access.
13. Dealing with false positives.  Why do they happen?
   1. Michael Henriksen had a great idea to just look for interesting files.  This approach, as well as a lot of others including the multiple modes of executions we added, have the propensity to produce a lot of false positives at scale.  There are a few places where regular expressions are used to match on file paths speaking to Michaels original approach.  There’s less risk of a regular expression matching on something out of context.  However, it doesn’t guarantee you’ve found a secret either because there’s no content inspection to help in that regard, so we added it.
   2. A mode 2 scan in gitrob that adds the additional step to look for content matches inside these interesting files.  However, it STILL doesn’t guarantee a valid finding.
   3. A mode 3 content scan has the highest probability of finding false positives, but also the highest probability of valid findings depending on the implementation of the regular expression.  At scale, the number of false positives is where many scanning tools, including gitrob and token-hunter, currently fall down.  The number of findings can be crippling and require hours of research and investigation.  
   4. Other tools like trufflehog and git-leaks rely on entropy checks, which is a measure of randomness.  These tools are awesome, but also imperfect much like gitrob and token-hunter.
      1. Is “p@ssword1” random?  Not really, it would score low on a Shannon entropy check.  Is it a valid password for a highly sensitive resource?  Maybe.  We’ve all seen weak passwords exposed, or set them ourselves at some point in our careers.
   5. GitLab personal access tokens are just a string of 20 characters.  Again, context is important as well as the accuracy and precision of the regular expression that is applied.
14. Tool matrix
   1. Here’s a look at some of the more common open and closed source tools for secrets hunting.  It’s not meant to be comprehensive, but it will give you an idea about which tool to go for depending on your context:  where do you need to look?  For what types of tokens are you looking?  Where are they likely to be found?
15. Remediation
   1. Remediation steps depend on the situation at hand.  It’s not “game over” once a secret gets leaked to a repository, though it can be tricky.
   2. Hopefully its just a single commit that can be reverted because it’s the only change included, but that’s almost never the case.
   3. Here is some information on remediation you can reference later if you like.
16. Future research and direction
   1. Find things faster
      1. Better filtering and storing of state
   2. Better signal to noise ratio
      1. Researching machine Learning approach to help reduce false positives that would apply to both gitrob and token-hunter.  Thanks to Juliet Wanjohi, who interned with us this summer and began researching what a machine learning model might look like for gitrob specifically.
17. Thank you very much, slides and demo recordings are available at our tech-notes repository on gitlab. 
18. A special thanks to my family and friends and the GitLab security team for supporting me in this, especially at this particular time.  Bye everyone, stay safe, and good luck.